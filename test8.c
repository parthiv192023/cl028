#include<stdio.h>

void main ()
{
  struct date
  {
    int d;
    int m;
    int y;
  };
  typedef struct date Date;

  struct emp
  {
    int id;
    char name[20];
    Date join;
    char post[20];
    double sal;
  };
  typedef struct emp Emp;
  
  Emp a;
  printf("\nEnter Employee Details\n");
  printf("Enter Employee ID: ");
  scanf("%d",&a.id);
  printf("Enter Employee Name: ");
  scanf("%s",&a.name);
  printf("Enter The Date Of Joining:\n");
  printf("Date: ");
  scanf(" %d",&a.join.d);
  printf("Month: ");
  scanf(" %d",&a.join.m);
  printf("Year: ");
  scanf(" %d",&a.join.y);
  printf("Enter Post Of Employee: ");
  scanf("%s",&a.post);
  printf("Enter Salary: ");
  scanf("%lf",&a.sal);
  
  printf("\nEmployee Details:\n");
  printf("Employee ID: %d\n",a.id);
  printf("Name: %s\n",a.name);
  printf("Date Of Joining: %d/%d/%d \n",a.join.d,a.join.m,a.join.y);
  printf("Post Of Employee: %s\n",a.post);
  printf("Salary: %.2lf\n",a.sal);
}