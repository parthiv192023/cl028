#include<stdio.h>
int main()
{
float height;
int age;
char section;
printf("Enter height of a student\n");
scanf("%f",&height);
printf("Enter age of a student\n");
scanf("%d",&age);
printf("Enter section of a student\n");
scanf("%c",&section);
printf("student information\n");
printf("Height=%f Age=%d Section=%c",height,age,section);
return 0;
}

#include <stdio.h>

int main()
{
    float celsius, fahrenheit;

    /* Input temperature in fahrenheit */
    printf("Enter temperature in Fahrenheit: ");
    scanf("%f", &fahrenheit);

    /* Fahrenheit to celsius conversion formula */
    celsius = (fahrenheit - 32) * 5 / 9;

    /* Print the value of celsius */
    printf("%.2f Fahrenheit = %.2f Celsius", fahrenheit, celsius);

    return 0;
}

#include <stdio.h>  
   
int main() {  
    int a, b;  
    printf("Please Enter Two different values\n");  
    scanf("%d %d", &a, &b);  
    
    if(a > b) 
    {
        printf("%d is Largest\n", a);          
    } 
    else if (b > a)
    { 
        printf("%d is Largest\n", b);  
    } 
    else 
    {
	printf("Both are Equal\n");
    }
   
    return 0;  
}