#include<stdio.h>

struct student 
{
  char name[20];
  int rn;
  char sec[5];
  char dept[10];
  double fee;
  int tot_mrks;
};
typedef struct student stud;

stud input_details(int n)
{
    stud s;
    printf("\nEnter Student %d Details:\n",n);
    printf("Enter Name: ");
    scanf("%s",&s.name);
    printf("Enter Roll No: ");
    scanf("%d",&s.rn);
    printf("Enter Section: ");
    scanf("%s",&s.sec);
    printf("Enter Department: ");
    scanf(" %s",&s.dept);
    printf("Enter Fees: ");
    scanf(" %lf",&s.fee);
    printf("Enter Total Marks scored: ");
    scanf(" %d",&s.tot_mrks);
    return s;
}

stud compare(stud s1,stud s2)
{
   if(s1.tot_mrks>s2.tot_mrks)
   {
       return s1;
   }
   else
   {
       return s2;
   }
}

void display(stud s)
{
    printf("\nDetails Of The Highest Scoring Student:\n");
    printf("Name: %s\n",s.name);
    printf("Roll No.: %d\n",s.rn);
    printf("Section: %s\n",s.sec);
    printf("Department: %s\n",s.dept);
    printf("Fees: %.2lf\n",s.fee);
    printf("Total Marks Scored: %d\n",s.tot_mrks);
}

void main()
{
    stud s1,s2,s;
    s1=input_details(1);
    s2=input_details(2);
    s=compare(s1,s2);
    display(s);
}