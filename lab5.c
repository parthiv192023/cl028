#include <stdio.h>

int input()
{
    int n;
    printf("Enter the number of elements:");
    scanf("%d",&n); 
    return n;
}

int compute(int n)
{
    int array[n],sum=0,i;
    float avg;
    for(i=0;i<n;i++)
    {
        printf("Enter number %d:",i+1);
        scanf("%d",&array[i]);
        sum=sum+array[i];
    }
    avg=sum/n;
    return avg;
}

void output (int n,int avg)
{
    printf("Average of the given %d numbers is %d",n,avg);
}

void main ()
{
    int n,avg;
    n=input();
    avg=compute(n);
    output(n,avg);
    
}