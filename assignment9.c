#include<stdio.h>

struct stud
{
    char name[20];
    int rn;
    char sec[5];
    char dept[5];
};
typedef struct stud Student;

void main()
{
    Student a[10];
    int i;
    printf("Enter The Student Details\n");
    for(i=0;i<10;i++)
    {
        printf("\nEnter Student %d Details:\n",i+1);
        printf("Enter Name: ");
        scanf("%s",&a[i].name);
        printf("Enter Roll No.: ");
        scanf("%d",&a[i].rn);
        printf("Enter Section: ");
        scanf("%s",&a[i].sec);
        printf("Enter Department: ");
        scanf("%s",&a[i].dept);
    }
    for(i=0;i<10;i++)
    {
        printf("\nStudent %d Details:\n",i+1);
        printf("Name: %s\n",a[i].name);
        printf("Roll No.: %d\n",a[i].rn);
        printf("Section: %s\n",a[i].sec);
        printf("Department: %s\n",a[i].dept);
    }
}





#include<stdio.h>

struct author
{
    char fn[10];
    char ln[10];
};
typedef struct author Auth;

struct publish
{
    int year;
    int mnth;
};
typedef struct publish Pd;

struct book
{
    char name[20];
    Auth a;
    Pd p;
};
typedef struct book Book;

void main ()
{
    int i,n;
    printf("Enter The Number Of Books: ");
    scanf("%d",&n);
    Book b[n];
    
    for(i=0;i<n;i++)
    {
        printf("\nEnter Book %d Details:\n",i+1);
        printf("Enter Book Name: ");
        scanf(" %s",&b[i].name);
        printf("Enter Month And Year OF Publication:\n");
        printf("Year: ");
        scanf(" %d",&b[i].p.year);
        printf("Month: ");
        scanf("%d", &b[i].p.mnth);
        printf("Enter Author Details:\n");
        printf("First Name: ");
        scanf(" %s",&b[i].a.fn);
        printf("Last Name: ");
        scanf(" %s",&b[i].a.ln);
    }
    
    for(i=0;i<n;i++)
    {
        printf("\nBook %d Details:\n",i+1);
        printf("Book Name: %s\n",b[i].name);
        printf("Author: %s %s\n",b[i].a.fn,b[i].a.ln);
        printf("Published In: %d/%d\n",b[i].p.mnth,b[i].p.year);
    }
}