#include <stdio.h>

struct dimension
{
    int r;
    int c;
};

typedef struct dimension size;
size a;
int i,j;

size input()
{
    printf("Enter the number of rows in the matrix:");
    scanf("%d",&a.r);
    printf("Enter the number of columns in the matrix:");
    scanf("%d",&a.c);
    printf("\n");
    return a;
}

void input_array(size a, int array[a.r][a.c])
{
    for(i=0;i< a.r;i++)
    {
        for(j=0;j<a.c;j++)
        {
            printf("Enter the Row %d Column %d element: ",i+1,j+1);
            scanf("%d",&array[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void display(int array[a.r][a.c])
{
    printf("The given matrix is:\n");
   for(i=0;i<a.r;i++)
    {
        for(j=0;j<a.c;j++)
        {
            printf("%d",array[i][j]);
            printf("\t");
        }
        printf("\n");
    } 
    printf("\n");
}

void display_transpose(int array[a.r][a.c])
{
    printf("The transpose of the given matrix is:\n");
    for(i=0;i<a.c;i++)
    {
        for(j=0;j<a.r;j++)
        {
            printf("%d",array[j][i]);
            printf("\t");
        }
        printf("\n");
    } 
    printf("\n");
}

void main()
{
    a=input();
    int array[a.r][a.c];
    input_array(a,array);
    display(array);
    display_transpose(array);
}